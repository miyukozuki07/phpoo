<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $anio_Fabricacion;
	private $revisionVerificacion;

	//declaracion del método verificación
	public function verificacion(){
		
		if(intval($_POST['anio_Fabricacion']) < 1990){ //si el año de fabricacion es menor a 1990
			$this->revisionVerificacion = "No"; //Entonces no circula, el valor se guarda en la propiedad revisionVerificacion
		}

		elseif(intval($_POST['anio_Fabricacion']) >= 1990 and intval($_POST['anio_Fabricacion']) <= 2010){ //si el año de fabricacion es mayor o igual a 1990 y menor o igual a 2010
			$this->revisionVerificacion = "Revision"; //Entonces el carro esta en revisión
		}

		elseif(intval($_POST['anio_Fabricacion']) > 2010){ //si el año de fabricacion es mayor a el año 2010
			$this->revisionVerificacion = "Circula"; //Entonces el carro puede circular
		}
		
	}

	public function get_revisionVerificacion(){
		return $this->revisionVerificacion;
	}

}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();
$Carro1->verificacion();


if (!empty($_POST)){ //Se guarda los valores enviados por el usuario a traves del formulario y los valores se guardan en los atributos correspondientes.
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->anio_Fabricacion=$_POST['anio_Fabricacion'];
}




