<?php  
//declaracion de clase token
	class contrasenia{
		//declaracion de atributos
		private $nombre;
		private $contrasena;
		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
			$this->contrasena=$this->generateRandomString();
		}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
		public function mostrar(){
			
		}

        public function generateRandomString($length = 4){
            return substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        }

		//declaracion de metodo destructor
		public function __destruct(){
			//destruye la contraseña
            echo 'Hola '.$this->nombre.' esta es tu contraseña: '.$this->contrasena;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$contra1= new contrasenia($_POST['nombre']);
	$mensaje=$contra1->mostrar();
}


?>