<?php
include_once('transporte.php');

//declaracion de la clase hijo o subclase Avion
class moto extends transporte{

    private $marcaMoto;
    
    //sobreescritura de constructor
    public function __construct($nom,$vel,$com,$marcaM){
        parent::__construct($nom,$vel,$com);
        $this->marcaMoto=$marcaM;
    }

    // sobreescritura de metodo
    public function resumenMoto(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Marca:</td>
                    <td>'. $this->marcaMoto.'</td>				
                </tr>';
        return $mensaje;
    }
}

?>