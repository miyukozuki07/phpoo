
<?php
//comando de inclusion con la ruta de las clases
include_once('../clases/ejercicio1/Carro.php');
include_once('../clases/ejercicio1/Moto.php');
?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	
	<!-- aqui puedes insertar el mesaje del servidor para Moto-->
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor2; ?>" readonly>
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor1; ?>" readonly>
	

	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Carro</h1></header><br>
	<form method="post">
		<div class="form-group row">

			<label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
			<div class="col-sm-4">
				<input class="form-control" type="color" name="carroColor" id="CajaTexto1">
			</div>
		
	</div>		
		
	<div class="container" style="margin-top: 4em">
	
	<header><h1>Moto</h1></header><br>
		<div class="form-group row">
			<div><label class="col-sm-3" for="CajaTexto2">Color de la Moto:</label></div>
		 	<div class="col-sm-4">
				<input class="form-control" type="color" name="motoColor" id="CajaTexto2">
			</div>
				
			<br>
			<div><label class="col-sm-3" for="CajaTexto3">Modelo de la Moto:</label></div>
		 	<div class="col-sm-4">
				<input class="form-control" type="text" name="motoModelo" id="CajaTexto3">
			</div>
			<br>

		<!-- inserta aqui los inputs para recibir los atributos del objeto-->				
		</div>
	
		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>
	</div>
	
</body>
</html>

